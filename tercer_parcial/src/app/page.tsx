"use client"
import * as React from 'react';
import Image from "next/image";
import styles from "./page.module.css";
import { Nav } from "@/componets/Nav";
import Grid from '@mui/material/Grid';
import { Box } from "@mui/material";
import { Hero } from "@/componets/BoxOne";
import {Typography} from "@mui/material";
import Button from '@mui/material/Button';
import BoxTree from '@/componets/BoxTree';
import { getCharacters } from "@/service/RickAndMorty"
import { useEffect, useState } from "react"
import { useRickAndMorty } from '@/hooks/useRick';
import { Loading } from '@/componets/Loading';
import { useEpisodesRick } from '@/hooks/useEpisodesRick';
import ActionAreaCard from '@/componets/ActionAreaCard';

export default function Home() {
  const { characters } = useRickAndMorty()
  const { episodes } = useEpisodesRick()
    
  return (
    <main className={styles.main}>
      <Nav></Nav>
      <Box className="secionOne" sx={{display:"flex", margin:0}}>
        <Typography sx={{color:""}} variant="h1" gutterBottom> Top scorer to the final match</Typography>
        <Box className="imageOne" sx={{padding:45,flexDirection:"column" }}>
          
        <Typography variant="body1" gutterBottom>
        The EuroLeague Finals Top Scorer is the individual award for the player that gained the highest points in the EuroLeague Finals
        
      </Typography>
      <Box sx={{display:"flex"}}>
        <Button variant="contained">Contained</Button>
        </Box>
        </Box>
        <Box sx={{}}>
      <Hero image="/images/one.svg" title="addas"></Hero>
      <Hero image="/images/one.svg" title="addas"></Hero>
        </Box>
      </Box>
      <Box>
      <Grid container spacing={{ xs: 2, md: 1 }} columns={{ xs: 2, sm: 1, md: 12 }}>
  {Array.from(Array(4 )).map((_, index) => (
    <Grid item xs={2} sm={3} md={3} key={index}>
      <Image src="/images/one.svg" width={300} height={300} alt=""></Image>
      <Box className="text" sx={{color:"linear-gradient(0deg, rgba(184,194,206,1) 0%, rgba(38,38,38,1) 100%)"}}>
      <Typography variant="h2" gutterBottom>iudhuashiduh</Typography>
      </Box>
    </Grid>
  ))}
</Grid>

      </Box>
        
        
        <Grid container spacing={2}>
              <Grid item xs={12}>
                <Box sx={{height:"70px",display: "flex",alignItems:"center", justifyContent: "space-between", marginBottom: "20px"}}>
                  <Typography variant="h5" gutterBottom className='pagination-item'>
                    Browse the category
                  </Typography>
                </Box>
              </Grid>
              {characters?.results.map((item: any, index: number) =>{
          const splitUrl = item.url.split('/')
          const id = splitUrl[splitUrl.lenght - 2]
          return(
            <Grid key={index} item sx={{display:"flex", alignItems:"center"}} >
              <BoxTree
              avatar={item.name.split('')[0]}
              title={item.name}
              image={item.image}
              name={item.species}
              date={item.location.name}
              ></BoxTree>
            </Grid>
          )
        })}
              {episodes?.results.map((item: any, index:number)=>{
        return(
          <Grid key={index} item xs={12} sm={12} md={6} lg={3} xl={3}>
            <ActionAreaCard
            image={item.image}
            title={item.name}
            text={item.species}
            ></ActionAreaCard>
          </Grid>
        )
        } )}
            </Grid>

    </main>
  );
}
