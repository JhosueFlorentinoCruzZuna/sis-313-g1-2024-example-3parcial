import { instancerRickAndMorty } from "@/configuration/confiRick"

export const getCharacters = async ()=>{
    const response = await instancerRickAndMorty.get('/character')
    return response.data
}
export const getEpisodes = async ()=>{
    const response = await instancerRickAndMorty.get('/episode?page=2')
    return response.data
}