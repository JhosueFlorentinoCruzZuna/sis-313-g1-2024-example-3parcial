import { getEpisodes } from "@/service/RickAndMorty"
import { useEffect, useState } from "react"

export const useEpisodesRick = () => {
    const [episodes,setEpisodesRick] = useState<any>()

    const fetchData = async () => {
        try{
            const responseEpisodes = await getEpisodes()
            setEpisodesRick(responseEpisodes)
        }catch(e){
            console.log(e)
        }
        
    }
    useEffect(() => {
        fetchData()
    },[])

    return {episodes}
}