import { getCharacters } from "@/service/RickAndMorty"
import { useEffect, useState } from "react"

export const useRickAndMorty = () => {
    const [characters,setCharacters] = useState<any>()

    const fetchData = async () => {
        try{
            const responseCharacters = await getCharacters()
            setCharacters(responseCharacters)
        }catch(e){
            console.log(e)
        }
        
    }
    useEffect(() => {
        fetchData()
    },[])

    return {characters}
}