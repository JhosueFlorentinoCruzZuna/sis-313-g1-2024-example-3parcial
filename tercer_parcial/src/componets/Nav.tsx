import { Logo } from "./Logo"
import { Box } from "@mui/material"
import Breadcrumbs from '@mui/material/Breadcrumbs';
import Link from '@mui/material/Link';
import Button from '@mui/material/Button';
import DeleteIcon from '@mui/icons-material/Delete';
import SendIcon from '@mui/icons-material/Send';
import Stack from '@mui/material/Stack';
import { SearchSharp } from "@mui/icons-material";

export const Nav=()=>{
    return(
        <Box sx={{display:'flex',justifyContent:'space-evenly', alignItems:'center'}}>
            <Logo></Logo>
            <Breadcrumbs separator=" " aria-label="breadcrumb">
        <Link underline="hover" color="black" href="/">
        Home
        </Link>
        <Link
          underline="hover"
          color="inherit"
          href="/material-ui/getting-started/installation/"
        >
        Category
        </Link>
        <Link underline="hover" color="inherit" href="/">
        Trending News
        </Link>
        <Link underline="hover" color="inherit" href="/">
        Recent News
        </Link>
        <Link underline="hover" color="inherit" href="/">
        Clubs Ranking
        </Link>
        <Link underline="hover" color="inherit" href="/">
        Sports Article
        </Link>
        
      </Breadcrumbs>
      <Stack direction="row" spacing={1}>
      <Button sx={{background:"#B8C2CE", color:"#ffff", border:"none"}} variant="outlined" startIcon={<SearchSharp />}>
      Search
      </Button>
    </Stack>

        </Box>
    )
}