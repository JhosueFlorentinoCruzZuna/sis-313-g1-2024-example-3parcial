import { Card, CardContent, CardMedia, Typography } from "@mui/material"
import React from "react"

interface Hr {
    image:string,
    title:string,
}

export const Hero = ({image,title}:Hr) => {
    return (
        <React.Fragment>
            <Card className="card">
                <CardMedia
                    component="img"
                    image={image}
                    className="card-media-img-languages"

                />
                <CardContent>
                    <Typography sx={{ fontSize: 40 }} color="text.secondary" gutterBottom>
                        {title}
                    </Typography>
                </CardContent>
            </Card>
        </React.Fragment>
    )
}